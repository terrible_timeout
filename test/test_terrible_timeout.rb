require 'test/unit'
require 'terrible_timeout'

class TestTerribleTimeout < Test::Unit::TestCase
  def test_run_nil
    assert_equal :true, TerribleTimeout.run(nil) { :true }
  end

  def test_expire
    assert_raises(TerribleTimeout::ExecutionExpired) do
      TerribleTimeout.run(0.01) { sleep 1 }
    end
  end

  def test_succeed
    assert_equal :ok, TerribleTimeout.run(1) { :ok }
  end

  def test_torture
    Thread.abort_on_exception = true
    assert_equal :ok, TerribleTimeout.run(1) { :ok }
    100000.times do
      assert_equal :ok, TerribleTimeout.run(1) { :ok }
    end
    test_expire
  end if ENV["TEST_TORTURE"]
end
