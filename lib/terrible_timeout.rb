# -*- encoding: binary -*-
require 'thread'
# License: GPLv3
#
# This is extracted from the Rainbows::ThreadTimeout middleware
# distributed with {Rainbows!}[http://rainbows.rubyforge.org/]
# This is INCOMPATIBLE with Rainbows::ThreadTimeout
#
# It behaves like Timeout.timeout in the Ruby standard library, but:
#
# * does not support nesting timeouts
# * only spawns one Thread per-process instead of one thread per-timeout
# * is explicitly incompatible with anything that else that uses Thread#raise
#
#     TerribleTimeout.run(666) do
#       puts "HELLO WORLD"
#     end
#
# You should read and understand all the code and all the caveats of this
# module before using it.
#
# == Caveats
#
# Badly-written C extensions may not be timed out.  Audit and fix
# (or remove) those extensions before relying on this module.
#
# Do NOT assume "ensure" clauses will fire properly
#
# Do NOT mix this with an existing Timeout.timeout block
#
# Do NOT use Rainbows::ThreadTimeout with this
#
# Do NOT nest invocations of this.
#
# This will behave badly if system time is changed since Ruby
# does not expose a monotonic clock for users, so don't change
# the system time while this is running.  All servers should be
# running ntpd anyways.
#
# You probably shouldn't use this (nor Timeout, nor Rainbows::ThreadTimeout).

module TerribleTimeout

  # we subclass Exception to get rid of normal StandardError rescues
  # in app-level code.  timeout.rb does something similar
  # You can rescue TerribleTimeout::ExecutionExpired explicitly
  # in your code to catch timeout errors
  class ExecutionExpired < Exception
  end

  # :stopdoc:

  # This is the main datastructure for communicating Threads eligible
  # for expiration to the watchdog thread.  If the eligible thread
  # completes its job before its expiration time, it will delete itself
  # @active.  If the watchdog thread notices the thread is timed out,
  # the watchdog thread will delete the thread from this hash as it
  # raises the exception.
  #
  # key: Thread to be timed out
  # value: Time of expiration
  @active = {}

  # Protects all access to @active.  It is important since it also limits
  # safe points for asynchronously raising exceptions.
  @lock = Mutex.new

  # watchdog thread, started lazily
  @watchdog = nil

  @sleep_cond = ConditionVariable.new

  # The watchdog thread is the one that does the job of killing threads
  # that have expired.  This method is only called when @lock acquired
  def self.start_watchdog
    @watchdog = Thread.new do
      begin
        # any far future time < 31 days should work...
        next_expiry = Time.now + 666666

        # We always lock access to @active, so we can't kill threads
        # that are about to release themselves from the eye of the
        # watchdog thread.
        @lock.synchronize do
          now = Time.now
          @active.delete_if do |thread, expire_at|
            # We also use this loop to get the maximum possible time to
            # sleep for if we're not killing the thread.
            if expire_at > now
              next_expiry = expire_at if next_expiry > expire_at
              false
            else
              # Terminate execution and delete this from the @active
              thread.raise(ExecutionExpired)
              true
            end
          end
        end

        # sleep until the next known thread is about to expire.
        sec = next_expiry - Time.now
        sec > 0.0 ? timed_wait(sec) : Thread.pass # give other threads a chance
      rescue => e
        warn "#{e.message} (#{e.class})"
      end while true # we run this forever
    end
  end

  def self.timed_wait(sec)
    @lock.synchronize do
      @sleep_cond.wait(@lock, sec)
    end
  end

  # :startdoc:

  # Runs the specified block of code and (hopefully) have it terminate
  # before +maxtime+
  #
  # See TerribleTimeout for an example of how to use this
  def self.run(maxtime)
    return yield if nil == maxtime
    Numeric === maxtime or raise TypeError, "not Numeric: #{maxtime.inspect}"

    # Once we have this lock, we ensure two things:
    # 1) there is only one watchdog thread started
    # 2) we can't be killed once we have this lock, it's unlikely
    #    to happen unless maxtime is really low and the machine
    #    is very slow.
    @lock.lock

    # we're dead if anything in the next two lines raises, but it's
    # highly unlikely that they will, and anything such as NoMemoryError
    # is hopeless and we might as well just die anyways.
    start_watchdog unless @watchdog && @watchdog.alive?
    @active[Thread.current] = Time.now + maxtime
    begin
      # Wakeup watchdog from a long sleep and force it to recalculate a
      # proper one.  Should be harmless if it's not sleeping.
      @sleep_cond.signal

      # It is important to unlock inside this begin block
      # Mutex#unlock really can't fail here since we did a successful
      # Mutex#lock before
      @lock.unlock

      # Once the Mutex was unlocked, we're open to Thread#raise from
      # the watchdog process.  This is the main place we expect to receive
      # Thread#raise.
      yield
    ensure
      # I's still possible to receive a Thread#raise here from
      # the watchdog, but that's alright, the "rescue ExecutionExpired"
      # line will catch that.
      @lock.synchronize { @active.delete(Thread.current) }
      # Thread#raise no longer possible here
    end
  end
end
